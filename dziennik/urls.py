#coding: utf-8
from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'dziennik.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    # WYRAŻENIA REGULARNE (REGEXP, REGULAR EXPRESSIONS)
    # regular-expressions.info
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'zaliczenie.views.home', name="home"),
    url(r'^zaliczenie/', include('zaliczenie.urls')),
    
)
